use crate::{Item, Items};

use yaml_rust::{Yaml, YamlLoader};

impl Items {
    pub fn serialize(s: &str) -> Self {
        Self::parse_yaml(YamlLoader::load_from_str(s).expect("failed to parse yaml")[0].clone().into_vec().expect("Base not an array"))
    }
    fn parse_yaml(y: Vec<Yaml>) -> Self {
        Items(y.into_iter().map(Item::parse_yaml).collect())
    }
}
impl Item {
    fn parse_yaml(y: Yaml) -> Self {dbg!(y.clone());
        let (name, main) = y.as_hash().unwrap().into_iter().nth(0).unwrap();
        let (name, main) = (
            name.clone().into_string().unwrap(),
            main.clone().into_hash().unwrap(),
        );
        let notes = main
            .get(&Yaml::String("Note".to_string()))
            .and_then(|x| x.clone().into_string());
        let sub_items = main
            .get(&Yaml::String("Items".to_string()))
            .and_then(|x| x.clone().into_vec())
            .map(|x| Items::parse_yaml(x));
        Item {
            name,
            notes,
            sub_items,
        }
    }
}
