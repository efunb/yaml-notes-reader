use crate::Items;
use handlebars::Handlebars;
use serde::{Deserialize, Serialize};

impl Items {
    pub fn render(&self) -> String {
        render_hbs(
            include_str!("../templates/main.hbs"),
            &json!({ "body": dbg!(self.render_items()) }),
        )
    }
    fn render_items(&self) -> String {
        render_hbs(
            include_str!("../templates/items.hbs"),
            dbg!(&json!({ "body": self.0.iter().map(|x|  render_hbs(
                include_str!("../templates/item.hbs"),
                &json!({
                    "name": x.name,
                    "notes": x.notes, 
                    "sub_items": x.sub_items.as_ref().map(|x|x.render_items())
                }),
            )).collect::<Vec<String>>().join("")})),
        )
    }
}

fn render_hbs<T: Serialize>(template_string: &str, data: &T) -> String {
    Handlebars::new()
        .render_template(template_string, data)
        .unwrap()
}
